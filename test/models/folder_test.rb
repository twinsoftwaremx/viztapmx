# == Schema Information
#
# Table name: folders
#
#  id               :integer          not null, primary key
#  name             :string(255)
#  parent_folder_id :integer
#  user_id          :integer
#  created_at       :datetime
#  updated_at       :datetime
#

require 'test_helper'

class FolderTest < ActiveSupport::TestCase

  test "get folders by user" do

    folders = users(:rodolfo).folders 

    assert_equal(2, folders.count)

  end

end
