# == Schema Information
#
# Table name: questions
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  question_text :text
#  type_id       :integer
#  folder_id     :integer
#  user_id       :integer
#  created_at    :datetime
#  updated_at    :datetime
#

class Question < ActiveRecord::Base
  belongs_to :type, class: 'QuestionType'
  belongs_to :folder
  belongs_to :user
end
