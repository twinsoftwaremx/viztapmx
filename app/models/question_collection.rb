# == Schema Information
#
# Table name: question_collections
#
#  id            :integer          not null, primary key
#  collection_id :integer
#  question_id   :integer
#  sort_num      :integer
#  visible       :boolean
#  created_at    :datetime
#  updated_at    :datetime
#

class QuestionCollection < ActiveRecord::Base
  belongs_to :collection
  belongs_to :question
end
