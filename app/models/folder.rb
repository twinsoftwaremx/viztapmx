# == Schema Information
#
# Table name: folders
#
#  id               :integer          not null, primary key
#  name             :string(255)
#  parent_folder_id :integer
#  user_id          :integer
#  created_at       :datetime
#  updated_at       :datetime
#

class Folder < ActiveRecord::Base
  belongs_to :parent_folder, class: 'Folder'
  belongs_to :user
  has_many :questions
end
