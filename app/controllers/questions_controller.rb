class QuestionsController < ApplicationController

  before_action :authenticate_user!
  before_action :set_record, only: [:show, :edit, :update, :destroy]
  before_action :fill_tables, only: [:new, :edit]

  def index
  end

  def show
  end

  def new
    @question = Question.new
    folder = Folder.find(params[:folder_id])
    @question.folder = folder
  end

  def create
    @question = Question.new(question_params)
    if @question.save
      redirect_to edit_folder_path(@question.folder)
    else
      render new_question_path
    end
  end

  def edit
  end

  def update
  end

  def destroy
  end

  private

    def set_record
      @question = Question.find(params[:id])
    end

    def question_params
      params.require(:question).permit(:name, :folder_id, :user_id)
    end

    def fill_tables
      @types = QuestionType.all
    end
end
