class FoldersController < ApplicationController

  before_action :authenticate_user!
  before_action :set_record, only: [:show, :edit, :update, :destroy]

  def index
    @folders = current_user.folders
  end

  def show
  end

  def new
    @folder = Folder.new
  end

  def create
    @folder = Folder.new(folder_params)
    @folder.user = current_user
    if @folder.save
      redirect_to folders_path
    else
      render new_folder_path
    end
  end

  def edit
  end

  def update
    @folder = Folder.find(params[:id])
    if @folder.update(folder_params)
      redirect_to folders_path
    else
      render edit_folder_path(@folder)
    end
  end

  def destroy
  end

  private

    def set_record
      @folder = Folder.find(params[:id])
    end

    def folder_params
      params.require(:folder).permit(:name, :parent_folder_id, :user_id)
    end
end
