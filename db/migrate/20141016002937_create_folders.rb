class CreateFolders < ActiveRecord::Migration
  def change
    create_table :folders do |t|
      t.string :name
      t.references :parent_folder, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
