class CreateQuestionCollections < ActiveRecord::Migration
  def change
    create_table :question_collections do |t|
      t.references :collection, index: true
      t.references :question, index: true
      t.integer :sort_num
      t.boolean :visible

      t.timestamps
    end
  end
end
