class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :name
      t.text :question_text
      t.references :type, index: true
      t.references :folder, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
